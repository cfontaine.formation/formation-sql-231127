USE bibliotheque;

-- Regroupement avec Group By

-- Obtenir le nombre de livres par année
-- Avec GROUP BY, on regroupe les lignes de la table par année et on compte le nombre de ligne pour chaque année
SELECT annee, count (id) FROM livres GROUP BY annee;

-- Obtenir le nombre de livres policier(genre=1) par année,  pour les années qui comportent au moins 3 livres
SELECT annee, count (id) AS nb_livre FROM livres WHERE genre=1 
GROUP BY annee HAVING  nb_livre>=3;

-- Obtenir le nombre d'auteur par pays
SELECT pays.nom,count(auteurs.id) FROM auteurs 
INNER JOIN pays ON pays.id=auteurs.nation
GROUP BY pays.nom;

-- Le nombre de livre par genre, pour les genres qui comportent au moins 5 livres
-- HAVING -> condition (idem where) après le regroupement, WHERE se trouve toujours avant le regroupement
SELECT genres.nom ,count(livres.id) AS nb_livres FROM livres
INNER JOIN genres ON genres.id=livres.genre
GROUP BY livres.genre HAVING nb_livres>=5 ORDER BY nb_livres DESC;

-- Le nombre de livre par auteur pour les auteurs qui ont écrit au moins 3 livres
SELECT auteurs.prenom, auteurs.nom, count(titre) AS nb_livre FROM auteurs
INNER JOIN livre2auteur ON auteurs.id=livre2auteur.id_auteur
INNER JOIN livres ON livres.id=livre2auteur.id_livre
GROUP BY auteurs.nom
HAVING nb_livre>=3
ORDER BY nb_livre DESC;

-- Exercice Group BY

USE world;
-- Nombre de pays par continent
SELECT continent,count(code) FROM country GROUP BY continent;

-- Afficher les continents et leur la population totale classé du plus peuplé au moins peuplé
SELECT continent, sum(population) AS population_totale
FROM country GROUP BY continent
ORDER BY population_totale DESC;

-- Nombre de langue officielle par pays - classé par nombre de langue officielle
SELECT name , count(countrycode) AS num_language FROM country
INNER JOIN countrylanguage ON code=countrycode 
WHERE isofficial ='T'
GROUP BY country.name ORDER BY num_language, name; 

USE elevage;

-- Quelles sont les races dont nous ne possédons aucun individu
SELECT Race.nom
FROM Race
LEFT JOIN Animal ON Animal.race_id = Race.id
GROUP BY Race.nom
HAVING COUNT(Animal.id) = 0;

USE exemple;

CREATE TABLE ventes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom_vendeur VARCHAR(30),
	annee INT,
	vente double
);

INSERT INTO ventes(nom_vendeur,annee,vente)
VALUES 
('Paul',2020,10000),
('Patrick',2020,11000),
('Paola',2020,15000),
('Paul',2021,8000),
('Patrick',2021,10000),
('Paola',2021,5000),
('Paul',2022,14000),
('Patrick',2022,5000),
('Paola',2022,16000),
('Paul',2019,12000),
('Patrick',2019,10000),
('Paola',2019,4000),
('Paul',2018,8000),
('Patrick',2018,10000),
('Paola',2018,5000),
('Paul',2017,1000),
('Patrick',2017,5000),
('Paola',2017,6000);

-- Fonction de fenetrage

-- Clause over
-- OVER() -> la partition est la table compléte
SELECT annee, nom_vendeur,vente ,SUM(vente) OVER() FROM ventes;

-- OVER(PARTITION BY annee) -> les partitions correspondent aux annees
SELECT annee, nom_vendeur,vente ,
SUM(vente) OVER(PARTITION BY annee) FROM ventes;

SELECT annee, nom_vendeur,vente ,
SUM(vente) OVER(PARTITION BY annee) 
,SUM(vente) OVER() 
,round(avg(vente) OVER(PARTITION BY annee),1) FROM ventes;

-- OVER(PARTITION BY nom_vendeur) -> les partitions correspondent aux nom_vendeur
SELECT annee, nom_vendeur,vente ,
SUM(vente) OVER(PARTITION BY nom_vendeur) FROM ventes;


-- Fonction de fenêtrage d'agrégation
-- count, sum,min,max, avg
SELECT annee, nom_vendeur ,vente,SUM (vente) OVER(PARTITION BY nom_vendeur) FROM ventes;


-- Fonction de fenetrage de classement , il faut utiliser ORDER BY dans la partition
-- ROW_NUMBER() -> numérote 1, 2, ... les lignes de la partitions
SELECT annee,nom_vendeur,vente ,
ROW_NUMBER() OVER(PARTITION BY nom_vendeur ORDER BY vente,annee DESC) FROM ventes;

-- RANK() -> idem DENSE_RANK(), mais on aura des écarts dans la séquence des valeurs lorsque plusieurs lignes ont le même rang
-- 1 - 2 - 3 - 4 - 4 - 6
SELECT annee,nom_vendeur,vente ,RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC,annee) FROM ventes;

-- DENSE_RANK() -> Attribue un rang à chaque ligne de sa partition en fonction de la clause ORDER BY
-- 1 - 2 - 3 - 4 - 4 - 5
SELECT annee,nom_vendeur,vente ,DENSE_RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

-- PERCENT_RANK() -> Calculer le rang en pourcentage (rank - 1)/(rows - 1)
SELECT annee, nom_vendeur ,vente, PERCENT_RANK() OVER(PARTITION BY nom_vendeur ORDER BY vente DESC ) FROM ventes;

-- NTILE(n)-> Distribue les lignes de chaque partition de fenêtre dans un nombre spécifié de groupes classés
SELECT annee,nom_vendeur,vente ,NTILE(3) OVER(PARTITION BY nom_vendeur ORDER BY vente DESC) FROM ventes;

--  Fonction de fenetrage de valeur 
-- LAG -> Renvoie la valeur de la ligne avant la ligne actuelle dans une partition
-- vente	lag(vente)
-- 16000
-- 15000	16000
SELECT annee,nom_vendeur,vente ,LAG(vente)OVER(PARTITION BY nom_vendeur ORDER  BY vente DESC)FROM ventes;

-- LEAD -> Renvoie la valeur de la ligne après la ligne actuelle dans une partition
-- vente	lead(vente)
-- 16000	5000
-- 5000		15000
SELECT annee,nom_vendeur,vente ,Lead(vente)OVER(PARTITION BY nom_vendeur ORDER  BY vente DESC) FROM ventes;

-- FIRST_VALUE ->Renvoie la valeur de l'expression spécifiée par rapport à la première ligne de la frame
SELECT annee,nom_vendeur,vente, FIRST_VALUE(vente)OVER(PARTITION BY nom_vendeur) FROM ventes;

-- LAST_VALUE -> Renvoie la valeur de l'expression spécifiée par rapport à la dernière ligne de la frame
SELECT annee,nom_vendeur,vente ,LAST_VALUE(vente)OVER(PARTITION BY nom_vendeur) FROM ventes;

-- NTH_VALUE -> Renvoie la valeur de l'argument de la nème ligne
SELECT annee,nom_vendeur,vente ,NTH_VALUE(vente,3) OVER(PARTITION BY nom_vendeur ) FROM ventes;

-- CUME_DIST -> Calcule la distribution cumulée d'une valeur dans un ensemble de valeurs
SELECT annee,nom_vendeur,vente ,CUME_DIST() OVER(ORDER BY annee) FROM ventes;

-- FRAME
SELECT annee,nom_vendeur,vente,
Sum(vente) Over(
	PARTITION BY annee
	ORDER BY annee 
	ROWS BETWEEN CURRENT ROW AND 1 FOLLOWING 
)
FROM ventes;
)

SELECT annee,nom_vendeur,vente,
Sum(vente) Over(
	PARTITION BY nom_vendeur 
	ORDER BY annee 
	ROWS BETWEEN 2 PRECEDING AND 1 FOLLOWING 
)
FROM ventes;

USE world;

SELECT sum(population) ,continent
FROM Country GROUP BY continent;

-- Afficher le nom du pays , le nom du continent , la population du pays,la population total du continent
SELECT name,continent,population, 
sum(population) OVER(PARTITION BY continent)
FROM Country;

-- Ajouter une colonne qui affiche le classement du pays par continent en fonction de la population du pays décroissante
SELECT name,continent,population,
sum(population) OVER(PARTITION BY continent), 
DENSE_RANK() OVER(PARTITION BY continent ORDER BY population DESC ) 
FROM Country;

USE exemple;

-- GROUP BY WITH ROLLUP
-- La clause ROLLUP génère plusieurs ensembles de regroupement en fonction 
-- des colonnes ou des expressions spécifiées dans la clause GROUP BY
SELECT COALESCE(annee,'Total='), SUM(vente) FROM ventes
GROUP BY annee WITH ROLLUP; 

SELECT COALESCE(annee,'Total='), Sum(vente), COALESCE(nom_vendeur,'sous-total année')  FROM ventes
GROUP BY annee, nom_vendeur
WITH ROLLUP ;

-- Grouping n'est pour l'innstant pas supporté par MariaDB mais est supporté par MySQL 8.x
-- GROUPING() NULL -> 1, sinon, elle renvoie 0
-- SELECT IF(GROUPING(annee)=1,'Total',annee), Sum(vente), 
-- IF(GROUPING(nom_vendeur)=1 AND GROUPING(annee)=0,'Sous-Total année',nom_vendeur),
-- GROUPING(annee),GROUPING(nom_vendeur) FROM ventes
-- GROUP BY annee, nom_vendeur
-- WITH ROLLUP ;
