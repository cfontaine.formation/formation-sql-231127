USE bibliotheque;


-- Appel de la procedure stockée
CALL auteur_vivant();

-- Afficher la liste des procédures stockées
SHOW PROCEDURE STATUS;

-- @var_global --> variable globale
-- Affecter une valeur à une variable globale
SET @var_global='test';

-- Afficher le contenu d'une variable 
SELECT @var_global;

CALL test_variable();
-- SELECT var_local; -- on ne peut accèder à une variable locale en dehors la procédure où elle est déclarée

-- Exemple passage de paramètre
-- 3 et 4 ->  paramètres en entrée
-- le résultat de l'addition va être placé dans la variable globale @var_global
CALL addition(3,4,@var_global);
SELECT @var_global;

CALL nb_livre_genre ('Policier',@var_global);
SELECT @var_global;

-- Exercice: procédure stockée
USE world;
CALL nb_ville_pays('NLD',@var_global);
SELECT @var_global; 

USE Bibliotheque;

-- Exemple de conditon IF
CALL parite(4,@var_global);
SELECT @var_global;

CALL parite(3,@var_global);
SELECT @var_global;

CALL nb_livre_genre2 ('Policier2',@var_global);
SELECT @var_global;

-- Exemple de condition CASE
CALL categorie_age_livre(2003,@var_global);
SELECT @var_global;

CALL categorie_age_livre(4000,@var_global);
SELECT @var_global;

CALL categorie_age_livre(1000,@var_global);
SELECT @var_global;

-- Exemple boucle WHILE
CALL somme_multi(5,@var_global);
SELECT @var_global;

-- Exemple Curseur
CALL auteurs_pays('FRANCE',@var_global);
SELECT @var_global;

-- Déclencheur
-- INSERT INTO genres(nom) VALUES('a'); -> Erreur: le nom du genre est < à 3 caratères
INSERT INTO genres(nom) VALUES('Théatre'); -- OK 
-- INSERT INTO genres(nom) VALUES('Théatre'); -> Erreur: le nom du genre existe déjà


-- Supression du déclencheur
DROP TRIGGER valide_genre;

-- Supression de toutes les procedures stockées
DROP PROCEDURE auteur_vivant;
DROP PROCEDURE test_variable;
DROP PROCEDURE nb_livre_genre
DROP PROCEDURE categorie_age_livre;
DROP PROCEDURE addition;
DROP PROCEDURE nb_ville_pays ;
DROP PROCEDURE auteurs_pays;
DROP PROCEDURE somme_multi;



