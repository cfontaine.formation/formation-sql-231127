USE exemple;

-- CREATE TEMPORARY TABLE -> table temporaire
-- Elle sera supprimée dés que l'on quitte la session
CREATE TEMPORARY TABLE utilisateurs
(
	id INT PRIMARY KEY AUTO_INCREMENT
	nom VARCHAR(100),
	prenom VARCHAR(100),
	email VARCHAR(255),
	date_naissance DATE
);

INSERT INTO utilisateurs(prenom,nom,email,date_naissance)
VALUES('john','doe','jd@dawan.com','1990-07-06');

-- affiche tous les utilisateur
SELECT * FROM utilisateurs;

-- On se deconnecte du SGBD
-- => la table n'existe plus
SELECT * FROM utilisateurs;

-- TABLE CTE
USE bibliotheque;

WITH auteur_vivant_cte AS(
	SELECT id,prenom,nom,naissance FROM auteurs
	WHERE deces IS NULL
)
-- SELECT * FROM livres;
SELECT * FROM auteur_vivant_cte WHERE naissance >'1950-01-01';

WITH auteur_vivant_cte AS(
	SELECT auteurs.id,prenom,auteurs.nom as nom_auteur,naissance,pays.nom AS nom_pays FROM auteurs
	INNER JOIN pays ON pays.id=auteurs.nation
	WHERE deces IS NULL
),
auteur_vivant_usa AS 
(
	SELECT prenom, nom_auteur FROM auteur_vivant_cte WHERE nom_pays='états-unis'
)
SELECT * FROM auteur_vivant_usa;

USE bibliotheque;

CREATE TABLE auteur_france 
SELECT id,prenom,nom,naissance FROM auteurs 
WHERE nation=4;

CREATE TEMPORARY TABLE auteur_france_tmp 
SELECT id,prenom,nom,naissance FROM auteurs 
WHERE nation=4;
SELECT  * FROM  auteur_france_tmp;

SHOW tables;