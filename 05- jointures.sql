USE bibliotheque;
-- Jointure Interne 

-- Avec WHERE
-- Afficher le titre, l'annee et le genre du livre
SELECT titre, annee, genre, genres.id,nom FROM livres, genres
WHERE livres.genre=genres.id;

-- INNER JOIN

-- Relation 1,N
-- On va joindre la table livres et la table genres en utilisant l'égalité entre
-- La clé primaire de genres -> id et la clé étrangère de livre -> genre
SELECT titre, annee,nom FROM livres
INNER JOIN genres ON livres.genre= genres.id;

-- Afficher le prénom et le nom de l'auteur et le nom du pays
SELECT prenom, auteurs.nom,pays.nom FROM auteurs
INNER JOIN pays ON auteurs.nation = pays.id;

-- Afficher le titre, l'année de sortie et le genre du livre qui sont sortie entre 1970 et 1980
-- pour les genres policier et drame trié par année croissante
SELECT titre,annee,nom FROM livres
INNER JOIN genres ON genre=genres.id
WHERE genres.nom IN ('policier','drame')
AND livres.annee BETWEEN 1970 AND 1980
ORDER BY annee; 

-- Relation n-n
-- On va joindre : la table livres et la table de jointure livre2auteur
-- On va joindre le resultat de la jointure précédente avec la table auteurs
-- Afficher le titre du livre, année de sortie du livre, le prénom et le nom de son auteurs
SELECT prenom, auteurs.nom, titre, annee, genres.nom FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs ON livre2auteur.id_auteur=auteurs.id;

SELECT prenom, auteurs.nom, titre, annee, genres.nom FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs ON livre2auteur.id_auteur=auteurs.id
INNER JOIN genres ON livres.genre = genres.id;

USE world;
-- Afficher chaque nom de pays et le nom de sa capitale
SELECT country.name,city.name FROM city 
INNER JOIN country ON city.id = country.capital;

-- Afficher les nom de pays et le nom 
-- de ses villes classer par nom de pays puis par nom de ville en ordre alphabétique (ne pas afficher les pays sans ville)
SELECT country.name, city.name FROM country
INNER JOIN city ON country.Code = city.CountryCode
WHERE city.name IS NOT NULL
ORDER BY country.name,city.Name;

-- Afficher les noms de  pays, la  langue et le 
-- pourcentage  classé par pays et par  pourcentage décroissant
SELECT country.name, LANGUAGE,percentage FROM country
INNER JOIN countrylanguage ON country.Code = countrylanguage.CountryCode 
ORDER BY name, percentage DESC;

USE exemple;

-- Produit cartésien -> CROSS JOIN
-- On obtient toutes les combinaisons possible entre les 2 tables
-- Le nombre d'enregistrements obtenu est égal au nombre de lignes de la première table multiplié par le nombre de lignes de la deuxième table.
CREATE TABLE plats(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50)
);

CREATE TABLE boissons(
	id int PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50)
);
INSERT INTO plats(nom) VALUES
('Céréale'),
('Pain'),
('Oeuf sur le plat');

INSERT INTO boissons(nom) VALUES
('Café'),
('Thé'),
('Jus d''orange');

-- On obtient toutes les combinaisons possible entre les boissons et les plats
SELECT plats.nom AS plat,boissons.nom AS boisson FROM plats 
CROSS JOIN boissons;

USE bibliotheque;

-- Jointure externe: LEFT JOIN ou RIGTH JOIN
SELECT titre, nom FROM genres
LEFT JOIN livres ON genres.id=livres.genre;

SELECT titre, nom FROM genres
LEFT JOIN livres ON genres.id=livres.genre
WHERE titre IS NULL;

SELECT prenom,auteurs.nom,pays.nom FROM auteurs
RIGHT JOIN pays ON pays.id=auteurs.nation;

-- FULL JOIN
-- SELECT prenom,auteurs.nom ,pays.nom FROM auteurs
-- FULL JOIN pays ON pays.id = auteurs.nation ;

-- FULL JOIN n'est pas encore supporté par MySql/MariaDb
-- Mais on peut le réaliser avec cette requète.
SELECT prenom,auteurs.nom ,pays.nom FROM auteurs
LEFT JOIN pays ON pays.id = auteurs.nation
UNION
SELECT prenom,auteurs.nom ,pays.nom FROM auteurs
RIGHT JOIN pays ON pays.id = auteurs.nation; 

-- Jointure naturelle (MySQL/ MariaDB)
-- Il faut que la clé primaire et la clé étrangère doivent avoir le même nom

-- Changer le nom de la clé primaire  id -> genre
ALTER TABLE livres DROP CONSTRAINT fk_genres;

ALTER TABLE genres CHANGE id genre INT;

ALTER TABLE livres ADD CONSTRAINT fk_genres 
FOREIGN KEY(genre)
REFERENCES genres(genre);

-- Jointure naturelle
SELECT titre, annee, nom FROM livres
NATURAL JOIN genres;

-- Changer le nom de la clé primaire genre -> id
ALTER TABLE livres DROP CONSTRAINT fk_genres;

ALTER TABLE genres CHANGE genre id INT;

ALTER TABLE livres ADD CONSTRAINT fk_genres 
FOREIGN KEY(genre)
REFERENCES genres(id);

-- Auto jointure
-- Une table que l'on joindre avec elle même

-- Une auto joiture est souvent utilisé pour représenter
-- une hierarchie en SQL (relation parent->enfant)
USE exemple;
CREATE TABLE salaries(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom varchar(40),
	nom varchar(40),
	manager INT,
	
	CONSTRAINT fk_manager
	FOREIGN KEY(manager)
	REFERENCES salaries(id) 
);

INSERT INTO salaries(id,prenom,nom,manager) VALUES
(1,'John','Doe',NULL),
(2,'Jane','Doe',1),
(3,'Alan','Smithee',2),
(4,'Joe','Dalton',1),
(5,'yves','Roulot',4);

SELECT employes.prenom AS emp_prenom, employes.nom AS emp_nom,
managers.prenom AS mgmt_prenom ,managers.nom AS mgmt_nom
FROM salaries AS employes
LEFT JOIN salaries AS managers
ON employes.manager=managers.id;

-- Exercice: Jointure interne et externe
USE world;
-- Afficher: les noms de  pays, la  langue et le  pourcentage  classé par pays et par  pourcentage décroissant. mais on ne veut obtenir que les langues officielles
SELECT country.name, LANGUAGE,percentage FROM country
INNER JOIN countrylanguage ON country.Code = countrylanguage.CountryCode 
WHERE countrylanguage.IsOfficial ='T';
ORDER BY name, percentage DESC;

-- Afficher le nom des pays sans ville
SELECT country.name  FROM country
LEFT JOIN city ON country.Code = city.CountryCode 
WHERE city.name IS NULL; 

-- Afficher tous les pays qui parlent français
SELECT country.name FROM country
INNER JOIN countrylanguage ON country.Code = countrylanguage.CountryCode 
WHERE countrylanguage.language ='French';