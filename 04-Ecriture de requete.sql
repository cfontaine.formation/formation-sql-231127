-- Sélectionner la base des données bibliotheque
USE bibliotheque;

-- Obtenir le prénom et le nom de tous les auteurs
SELECT annee,titre FROM livres;

-- * -> pour obtenir toutes les colonnes
SELECT * FROM livres;

-- Quand il ya une ambiguitée sur le nom d'une colonne, on ajoute le nom de la table => nom_table.nom_colonne
SELECT prenom,pays.nom,auteurs.nom FROM auteurs,pays;

-- On peut mettre dans les colonnes d'un SELECT: une constante ou une colonne qui provient d'un calcul
SELECT titre, 'age=',2023-annee FROM livres;

-- ALias 
-- un alias permet de renommer temporairement une colonne ou une table dans une requête
-- colonne AS alias, AS est optionel => colonne Alias 
-- Sur les colonnes
SELECT titre AS titre_livre, 2023-annee AS age FROM livres;
SELECT titre titre_livre, 2023-annee age FROM livres;

-- sur les tables
SELECT a.prenom,a.nom  FROM auteurs AS a;
SELECT a.prenom,a.nom  FROM auteurs a;

-- DISTINCT -> permet d’éviter les doublons dans les résultats d’un SELECT
SELECT ALL prenom FROM auteurs; -- 39 lignes
SELECT prenom FROM auteurs; -- ALL est implicite 

SELECT DISTINCT prenom FROM auteurs; -- 37 lignes

-- Clause WHERE
-- La clause WHERE permet de sélectionner des lignes qui respectent une condition
-- Selection de tous les titres de livre qui sont sortie après 2000
SELECT titre,annee FROM livres WHERE annee>2000; 
SELECT titre,annee FROM livres WHERE 2023-annee<24; 

-- Les opérateurs logiques AND et OR permettent de combiner des conditions
-- avec l'opérateur AND, il faut que toutes les conditions soient vrai pour que la ligne soit sélectionnée
-- Selection des titres, de l'année de sortie du livre qui sont sorties entre 1950 et 1980
SELECT titre,annee FROM livres WHERE annee>1950 AND annee<1980;

SELECT titre,annee FROM livres WHERE annee>1950 AND genre=1;

-- avec l'opérateur OR, il faut qu'une des conditions soient vrai ou les 2 pour que la ligne soit sélectionnée
-- Selection de tous les titres et année de sortie de livre qui sont sortie en 1992 et en 1956
SELECT titre,annee FROM livres WHERE annee=1992 OR annee=1954;

SELECT titre,annee FROM livres WHERE annee<1950 OR annee>1980;

-- L'opérateur XOR ou exclusif ->  il faut qu'une des conditions soient vrai pour que la ligne soit sélectionnée
SELECT titre,annee FROM livres WHERE annee>1950 XOR genre=1;

-- L'opérateur NOT -> inverser la condition
SELECT titre,annee FROM livres WHERE NOT annee>1990;

-- Exercice requête simple
USE world;
-- Afficher toutes les colonnes et toutes les lignes de la table countrylanguage
SELECT * FROM countrylanguage;

-- Afficher le nom des villes et leur population
SELECT name, population FROM city;

-- Afficher la liste des noms des continents sans doublons qui se trouve de la table country
SELECT DISTINCT continent FROM country;

-- Afficher le nom des villes du district de Nagano
SELECT name FROM city WHERE district='Nagano' ;

-- Afficher les pays dont la surface est comprise entre 80000 et 100000 km2
SELECT name FROM country WHERE surfacearea>80000 AND surfacearea<100000;

-- Afficher les noms et la population des villes qui ont un code_country égal à ITA
-- et dont la population est supérieur à 300000 habitants
SELECT name, population FROM city WHERE countryCode = 'ITA' AND Population >300000 ;
-- Afficher le nom du pays, le continent, espérance de vie et produit national brut (gnp)
-- soit des pays qui ont une espérance de vie supérieur à 80 ans 
-- ou le pays européens qui un produit national brut supérieur à 1000000 d'euro
SELECT name,continent,lifeexpectancy,gnp FROM country 
WHERE lifeexpectancy>80 OR (continent='Europe' AND gnp>1000000);

USE bibliotheque;

-- l'opérateur IN permet de vérifier, si une colonne fait partie des valeurs d'un ensemble de valeurs définis
-- Sélection du titre et de l'année pour les livres sortie en 1992,1952 ou 1954
SELECT titre, annee FROM Livres WHERE annee IN (1992,1952,1954);

-- Sélection du prénom et du nom pour les auteurs qui ont pour prénom Pierre, James, Stephen
SELECT prenom,nom FROM auteurs WHERE prenom IN ('Pierre','James','Stephen');

-- l'opérateur BETWEEN est utilisé pour vérifier si une colonne fait partie d’un intervalle de données
-- Sélection des titres et l'année des livres qui sortie entre 1980 et 2000
SELECT titre, annee FROM livres WHERE annee BETWEEN 1980 AND 2000;

-- Sélection du prénom , du nom des auteurs dont le prénom est compris entre John et Pierre
SELECT prenom,nom FROM auteurs WHERE prenom BETWEEN 'John' AND 'Pierre';

-- Sélection du prénom , du nom  et de la date de naissance des auteurs qui sont nés entre le 10 janvier 1930 et  le 14 juillet 1951
SELECT nom, naissance FROM auteurs WHERE naissance BETWEEN '1930-01-10'AND '1951-07-14';

-- L’opérateur LIKE permet d’effectuer une recherche sur un modèle particulier
--  % représente 0,1 ou plusieurs caratères inconnues
--  _ représente un caratère inconnue

-- Sélection du titre des livres qui commence par d et qui fait 4 caractères (ne tient pas compte de la casse)
SELECT titre FROM livres WHERE titre LIKE 'd___';

-- Sélection du titre des livres qui commence par d et qui fait au moins 4 caractères
SELECT titre FROM livres WHERE titre LIKE 'd___%';

-- Sélection du titre des livres qui commence par d
SELECT titre FROM livres WHERE titre LIKE 'd%';

-- Sélection du prénoms pour les auteurs qui ont un prénom composé
SELECT prenom FROM auteurs WHERE prenom LIKE '%-%';

-- IS NULL permet de tester si une valeur est égal à NULL 	/!\ on ne peut pas utiliser = pour tester si un champs est NULL
-- Sélection du le prenom et le nom de la table auteurs pour les auteurs qui sont vivants => deces = NULL
SELECT prenom , nom FROM auteurs  WHERE deces IS NULL;

-- IS NOT NULL permet de tester, si une valeur est différente de NULL
-- Sélection du le prenom et le nom de la table auteurs pour les auteurs qui sont décédés
SELECT prenom , nom FROM auteurs  WHERE deces IS NOT NULL;

-- ORDER BY -> trier par ordre ascendant ASC (par défaut), ou par ordre décendant DESC
-- Selection de tous les livres triés par rapport à l'année par ordre décroissant et par titre par ordre croissant
SELECT titre,annee,genre FROM livres ORDER BY annee DESC,genre ,titre DESC;

SELECT nom, prenom, naissance FROM auteurs ORDER BY naissance;
-- LIMIT -> limiter le nombre de ligne du résultat

SELECT * FROM livres LIMIT 5; 

SELECT titre, annee FROM livres ORDER BY annee LIMIT 5 ;
SELECT titre, annee FROM livres ORDER BY annee LIMIT 5 OFFSET 6;

SELECT titre, annee FROM livres ORDER BY annee LIMIT 6,5; -- mysql uniquement

-- Sélectionner les 3 auteurs les plus anciens
SELECT prenom, nom FROM auteurs WHERE nation=1 
ORDER BY naissance LIMIT 3; 

USE world;
-- Afficher le nom et l'année d'indépendance des pays qui sont devenus indépendant en 1825, 1867, 1963 et  1993
SELECT name, indepyear FROM country WHERE IndepYear IN (1825, 1867, 1963 ,1993);

-- Afficher le nom, le continent et la population des pays commençant par la lettre c et contenant au moins 6 caractères,
--  classé par ordre décroissant de population :
SELECT name, continent,population FROM country WHERE name LIKE 'c_____%' ORDER BY Population DESC;

-- Afficher les 10 pays les plus peuplés (nom et population)
SELECT name,population  FROM country ORDER BY population DESC LIMIT 10;

-- Afficher le nom de la 3 ème ville la plus peuplé
SELECT name FROM city ORDER BY population DESC LIMIT 1 OFFSET 2;

-- Afficher les pays qui n'ont pas de capitale
SELECT name FROM country WHERE Capital IS NULL;
