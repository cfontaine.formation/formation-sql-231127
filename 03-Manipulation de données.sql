USE exemple;

-- Ajouter des données
-- 1
INSERT INTO marques VALUES(1,'Marque A','1987-03-07');
INSERT INTO marques VALUES('Marque AA','1987-03-07');

-- 2
INSERT INTO marques(nom,date_creation) VALUES('Marque B','1900-03-30');
INSERT INTO marques(nom) VALUES('Marque C');

-- 3
INSERT INTO articles(prix,nom_article,marque) VALUES 
(650.0,'TV 4K',3),
(150.0,'Disque SSD 2Go',1),
(30.0,'Souris',1);

-- INSERT INTO articles(prix,nom_article,marque) VALUES
-- (750.0,'TV 4K 60 pouces',300);

INSERT INTO fournisseurs(nom) VALUES
('Fournisseur 1'),
('Fournisseur 2');

INSERT INTO fournisseurs_articles (id_article,id_fournisseur) VALUES
(1,1),
(1,2),
(3,2);

-- Supprimer
DELETE FROM marques WHERE id=4;
-- DELETE FROM marques WHERE id=1;

DELETE FROM articles WHERE prix>200.0;

DELETE FROM fournisseurs_articles;

DELETE FROM articles;
INSERT INTO articles(prix,nom_article,marque) VALUES 
(650.0,'TV 4K',3);

SET FOREIGN_KEY_CHECKS =0;
TRUNCATE TABLE articles;
SET FOREIGN_KEY_CHECKS =1;

INSERT INTO articles(prix,nom_article,marque) VALUES 
(650.0,'TV 4K',3);

UPDATE articles SET prix=400.0 WHERE reference=1;

UPDATE articles SET prix=1.0 WHERE prix<50.0;

UPDATE articles SET prix=prix*1.20 WHERE prix>500.0;

UPDATE articles SET prix=0.0 ;
