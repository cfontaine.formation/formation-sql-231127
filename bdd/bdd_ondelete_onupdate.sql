CREATE TABLE marques_cascade(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL,
	date_creation DATE
);

CREATE TABLE articles_cascade(
	reference INT PRIMARY KEY AUTO_INCREMENT,
	description VARCHAR(255),
	prix DECIMAL(6,2) NOT NULL DEFAULT 10.0, 				
	marque INT,
	CONSTRAINT  FK_articles_marques_cascade
	FOREIGN KEY (marque)
	REFERENCES marques_cascade(id)
);

INSERT INTO marques_cascade(nom) VALUES 
('Marque A'),
('Marque B'),
('Marque C');


INSERT INTO articles_cascade(description,prix,marque) VALUES 
('Souris',30.0,1),
('Clavier AZERTY',20.0,1),
('TV 4K',600.0,2),
('Carte mère ',130.0,3),
('Carte graphique',500.0,3);
