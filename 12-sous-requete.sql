USE bibliotheque;

-- Requête imbriqué retourne un seul résultat
SELECT titre,annee FROM livres
WHERE annee=(
	SELECT round(avg(annee),0)
	FROM livres
	WHERE genre=1
);

SELECT titre, annee FROM livres
WHERE annee IN(
	SELECT annee FROM livres
	INNER JOIN genres ON livres.genre = genres.genre 
	WHERE genres.nom='horreur'
);

-- sous-requete corrélé
SELECT genres.genre, nom FROM genres
WHERE EXISTS(
	SELECT livres.id
	FROM livres
	WHERE genre=genres.genre
);

SELECT DISTINCT auteurs.prenom, auteurs.nom, genres.nom FROM auteurs
INNER JOIN livre2auteur ON livre2auteur.id_auteur = auteurs.id 
INNER JOIN livres ON livre2auteur.id_livre = livres.id 
INNER JOIN genres ON livres.genre= genres.genre 
WHERE EXISTS(
	SELECT livres.id
	FROM livres
	WHERE genre=genres.genre
);

SELECT DISTINCT auteurs.prenom, auteurs.nom, genres.nom FROM auteurs
INNER JOIN livre2auteur ON livre2auteur.id_auteur = auteurs.id 
INNER JOIN livres ON livre2auteur.id_livre = livres.id 
INNER JOIN genres ON livres.genre= genres.genre 


SELECT titre ,annee FROM livres
WHERE annee <> ALL(
			SELECT annee FROM livres
			INNER JOIN genres 
			ON livres.genre = genres.genre
			WHERE genres.nom='science-fiction'
);

-- ANY ou SOME 
SELECT titre ,annee FROM livres
WHERE annee < ANY(
			SELECT annee FROM livres
			INNER JOIN genres 
			ON livres.genre = genres.genre
			WHERE genres.nom='science-fiction'
);

USE world;
-- Tous les pays avec une capitale de plus d'un million d'habitant
SELECT name FROM country
WHERE capital IN (
SELECT city.ID  FROM city WHERE population> 1000000); 

-- Liste des pays ayant une langue parlée par moins de 10% de la population
SELECT name
FROM country
WHERE code IN (
    SELECT DISTINCT countrylanguage.CountryCode
    FROM countrylanguage
    WHERE countrylanguage.Percentage <10
);
