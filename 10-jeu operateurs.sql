USE exemple;

-- Union
-- Pas de doublon avec union
SELECT prenom,nom,adresse_personnel FROM employees
UNION 
SELECT prenom,nom,adresse_personnel FROM clients;

-- Union ALL autorise les doublons
SELECT prenom,nom,adresse_personnel FROM employees
UNION ALL
SELECT prenom,nom,adresse_personnel FROM clients;

SELECT prenom,nom,adresse_personnel,'emp' AS identifiant FROM employees
UNION 
SELECT prenom,nom,adresse_personnel,'clt' AS identifiant FROM clients;

-- Intersect
SELECT prenom,nom,adresse_personnel FROM employees
INTERSECT 
SELECT prenom,nom,adresse_personnel FROM clients;

-- Except
SELECT prenom,nom,adresse_personnel FROM employees
EXCEPT
SELECT prenom,nom,adresse_personnel FROM clients;

USE world;

-- Afficher la liste des pays et des villes ayant moins de 1000 habitant
SELECT name , 'country' AS ident FROM country WHERE population < 1000
UNION
SELECT name , 'city' AS ident FROM city WHERE population < 1000;

-- Liste des pays où on parle français ou anglais 
SELECT name FROM country 
INNER JOIN countrylanguage ON country.Code = countrylanguage.CountryCode 
WHERE LANGUAGE='french'
UNION
SELECT name FROM country 
INNER JOIN countrylanguage ON country.Code = countrylanguage.CountryCode 
WHERE LANGUAGE='english'

SELECT DISTINCT name FROM country 
INNER JOIN countrylanguage ON country.Code = countrylanguage.CountryCode 
WHERE LANGUAGE IN ('french','english');