-- un commentaire fin de ligne

# un commentaire fin de ligne

/*
 Commentaire sur 
 plusieurs
 lignes
 */

-- Créer un base de données exemple
CREATE DATABASE exemple;

-- Afficher l'ensemble des bases de données 
SHOW DATABASES;

-- Choisir la base de données exemple, comme base courante
USE exemple;

-- Supprimer une base de donnée
-- DROP DATABASE exemple;

-- Créer une table article
CREATE TABLE article(
	reference INT,
	prix DECIMAL(6,2) 
	description VARCHAR(255)
);

-- Supprimer la table article
-- DROP TABLE article ;

-- Lister les tables de la base de données
SHOW TABLES ;

-- Afficher la description de la table
DESCRIBE article;

-- Exercice: créer la table vols
CREATE TABLE vols(
	numero_vol INT,
	heure_depart DATETIME,
	ville_depart VARCHAR(200),
	heure_arrive DATETIME,
	ville_arrive VARCHAR(200)
);

-- Modifier une table -> ALTER TABLE
-- renommer une table (mysql/mariadb)
RENAME TABLE article TO articles;

-- Supprimer une colonne
ALTER TABLE articles DROP description;

-- Ajouter une colonne
ALTER TABLE articles ADD nom VARCHAR(20);

-- Modifier le type d'une colonne	VARCHAR(20) -> VARCHAR(60)
ALTER TABLE articles MODIFY nom VARCHAR(60);

-- Modifier le nom de la colonne (nom -> nom_article)
ALTER TABLE articles CHANGE nom nom_article VARCHAR(60);

-- Modifier une colonne pour ajouter NOT NULL au nom_article
ALTER TABLE articles MODIFY prix DECIMAL(6,2) NOT NULL;

-- Modifier une colonne pour ajouter une valeur par défaut 10.0 pour le prix de l'article
ALTER TABLE articles MODIFY prix DECIMAL(6,2) NOT NULL DEFAULT 10.0;


-- Clé primaire
-- Une clé primaire est la donnée qui permet d'identifier de manière unique une ligne dans une table

-- Modifier la table articles pour que la colonne reférence deviennent la clé primaire de la table
ALTER TABLE articles ADD CONSTRAINT pk_articles PRIMARY KEY(reference); 

-- Modifier la colonne référence pour ajouter AUTO_INCREMENT sur la clé primaire
ALTER TABLE Articles MODIFY reference INT AUTO_INCREMENT ;

DROP TABLE articles;

-- Création de la table articles directement avec la clé primaire, NOT NULL ... 
CREATE TABLE articles(
	reference INT PRIMARY KEY AUTO_INCREMENT,
	prix DECIMAL(6,2) NOT NULL DEFAULT 10, 	-- NOT NULL -> prix ne peut plus être égal à NULL, lorsque l'on ajoutera des données, on sera obligé de données un valeur à prix
											-- DEFAULT -> permet de définir une valeur par défaut autre que NULL
	nom_article VARCHAR(60)	 -- UNIQUE 		-- UNIQUE -> si on décommente UNIQUE, Il ne peut pas avoir de doublon pour le nom du produi
);

-- Exercice table pilotes
CREATE TABLE pilotes
(
	numero_pilote INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50) NOT NULL,
	nom VARCHAR(50) NOT NULL,
	nombre_heure_vol INT NOT NULL
);

-- Exercice table avions
CREATE TABLE avions
(
	numero_avion INT PRIMARY KEY AUTO_INCREMENT,
	modele VARCHAR(30) NOT NULL,
	capacite INT NOT NULL
);

-- Exercice table vols
-- Modifier la table vols pour que la colonne numero_vol deviennent la clé primaire de la table
ALTER TABLE vols ADD CONSTRAINT pk_vols PRIMARY KEY(numero_vol);

-- Ajouter AUTO_INCREMENT sur la clé primaire numero_vol
ALTER TABLE vols MODIFY numero_vol INT AUTO_INCREMENT;

-- Ajouter NOT NULL sur les colonnes ville_depart et ville_arrive
ALTER TABLE vols MODIFY ville_depart VARCHAR(200) NOT NULL;
ALTER TABLE vols MODIFY ville_arrive  VARCHAR(200) NOT NULL;

-- Relation entre table

-- Relation 1,N
CREATE TABLE marques(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL,
	date_creation DATE
);

-- On ajoute une colonne qui sera la clé étrangère
ALTER TABLE articles ADD marque INT;

-- Ajout d'une contrainte de clé étrangère 
-- clé étrangère colonne marque de article
-- elle fait référence à la clé primaire id de la table marques 
ALTER TABLE articles ADD
CONSTRAINT fk_articles_marques
FOREIGN KEY(marque)
REFERENCES marques(id);

-- Création de la table articles directement avec la contrainte de clé étrangère
-- DROP TABLE articles;
-- CREATE TABLE articles(
-- 	reference INT PRIMARY KEY AUTO_INCREMENT,
-- 	description VARCHAR(255),
-- 	prix DECIMAL(6,2) NOT NULL DEFAULT 10.0, 				
-- 	nom_article VARCHAR(60) NOT NULL UNIQUE,
-- 	marque INT,
-- 	CONSTRAINT  FK_articles_marques
-- 	FOREIGN KEY (marque)
-- 	REFERENCES marques(id)
-- );


-- Exercice: Faire la relation entre vols et pilote
-- - ajouter un colonne sur le coté "1" de la relation
-- - ajouter un contrainte de clé étrangère
-- - idem pour la relation entre vols et avion

-- Ajout d'un colonne à la table vols (clé étrangères vers la tables pilotes)
ALTER TABLE vols ADD pilote INT;

-- Ajout de clé étrangères entre vols et pilotes
ALTER TABLE vols ADD 
CONSTRAINT fk_vols_pilotes
FOREIGN KEY (pilote)
REFERENCES pilotes (numero_pilote);

-- Ajout d'un colonne à la table vols (clé étrangères vers la tables avions)
ALTER TABLE vols ADD avion INT;

-- Ajout de clé étrangères entre vols et avions
ALTER TABLE vols ADD 
CONSTRAINT fk_vols_avions
FOREIGN KEY (avion)
REFERENCES avions (numero_avion);

-- Relation n,n -> table de jointure
CREATE TABLE fournisseurs(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(50) NOT NULL
);

-- Création de la Table de jointure
CREATE TABLE fournisseurs_articles(
	id_article INT,		-- clé étrangère vers la TABLE articles
	id_fournisseur INT,	-- clé étrangère vers la TABLE fournisseurs
	
	CONSTRAINT fk_fournisseurs_articles
	FOREIGN KEY (id_fournisseur)
	REFERENCES fournisseurs(id),
	
	CONSTRAINT fk_articles_fournisseurs
	FOREIGN KEY (id_article )
	REFERENCES articles(reference),

	-- clé primaire composée (id_article,id_fournisseur)
	CONSTRAINT pk_articles_fournisseurs
	PRIMARY KEY(id_article,id_fournisseur)
);
