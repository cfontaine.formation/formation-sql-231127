USE bibliotheque;

-- Déclaration d'un procedure stockée
DELIMITER $	-- CHANGE le délimiteur de fin de ligne ; -> $
CREATE PROCEDURE auteur_vivant() COMMENT 'obtenir les auteurs vivant'
BEGIN
	SELECT prenom, nom,naissance FROM auteurs WHERE deces IS NULL;
END $ 
DELIMITER ;	-- CHANGE le délimiteur de fin de ligne $ -> ;

DELIMITER $
CREATE PROCEDURE test_variable()
BEGIN 
	-- déclaration d'une variable locale
	DECLARE var_local INT DEFAULT 10;
	-- affecter une valeur à une variable locale
	SET var_local=2;
	-- afficher une variable
	SELECT var_local;
	-- INTO -> affecter une variable avec le résultat d'une requête
	SELECT count(genre) INTO var_local FROM genres;
	SELECT var_local;
 	SELECT @var_global;
END $
DELIMITER ;

-- Passage de paramètre
-- - en entrée 2 entiers a et b
-- - en sortie un entier somme 
DELIMITER $
CREATE PROCEDURE addition(IN a INT, IN b INT,OUT somme INT)
BEGIN 
	SET somme=a+b;
END $
DELIMITER ;

-- nb_livre_genre -> la procédure stockée prend en paramètre l'id du genre 
-- et en sortie on obtient le nombre de livre qui correspond à ce genre 
DELIMITER $
CREATE PROCEDURE nb_livre_genre(IN nom_genre VARCHAR(50),OUT nb_livre INT)
BEGIN 
	SELECT count(livres.id) INTO nb_livre FROM livres
	INNER JOIN genres ON livres.genre=genres.id
	WHERE genres.nom= nom_genre;
END $
DELIMITER ;

-- Exercice Procédure Stockée
-- Créer une procédure qui a pour nom: nb_ville_pays en paramètre, on a:
--  - en entrée un entier code_country
--  - en sortie un entier nb_ville_pays
-- La procédure pour le code_country passé en paramètre va retourner le nombre de ville
DELIMITER $
CREATE PROCEDURE nb_ville_pays( IN code_country CHAR(3), OUT nb_ville_pays INT)
BEGIN
SELECT count(id) INTO nb_ville_pays FROM city WHERE countrycode=code_country COLLATE utf8mb4_general_ci;
END $
DELIMITER ;

-- Condition IF
-- La procédure prend en paramètre un entier et on retourne une chaine de caractère qui contient
-- paire ou impaire suivant la parité du paramètre d'entrée 
DELIMITER $
CREATE PROCEDURE parite(IN val INT, OUT str VARCHAR(10))
BEGIN
	IF val MOD 2=0 THEN 
		SET str='Paire';
	ELSE
		SET str='Impaire';
	END IF;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE nb_livre_genre2(IN nom_genre VARCHAR(50),OUT nb_livre INT)
BEGIN 
	SELECT count(livres.id) INTO nb_livre FROM livres
	INNER JOIN genres ON livres.genre=genres.genre
	WHERE genres.nom= nom_genre;
	IF nb_livre=0 THEN
		SET nb_livre = NULL;
	END IF;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE categorie_age_livre(IN annee_edition INT,OUT categorie_age VARCHAR(50))
BEGIN 
	DECLARE age_livre INT;
	IF annee_edition> YEAR(now()) THEN 
		SET categorie_age ='Erreur année';
	ELSE
		SET age_livre = YEAR(now())-annee_edition ;
		CASE
			WHEN annee_edition >2000 THEN 
				SET categorie_age=concat('Moderne (',age_livre,' ans)');
			WHEN annee_edition >1900 AND annee_edition<2000 THEN
				SET categorie_age=concat('20ème siècle (',age_livre,' ans)');
			ELSE 
				SET categorie_age=concat('Ancien (',age_livre,' ans)');
		END CASE;
	END IF;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE multiple(IN v INT,OUT m INT)
BEGIN 
	DECLARE i INT DEFAULT 0;
	SET m=0;
	WHILE i<v DO
		SET i=i+1;
		SET m=m+i;
	END WHILE;
END $
DELIMITER ;

DELIMITER $
CREATE PROCEDURE auteur_pays(IN nom_pays VARCHAR(50),OUT auteurs VARCHAR(1000))
BEGIN 
	DECLARE done INT DEFAULT 0;
	DECLARE auth VARCHAR(100) DEFAULT '';

  DECLARE cur_auteur CURSOR FOR 
  		SELECT concat(prenom,' ',auteurs.nom) FROM auteurs
  		INNER JOIN pays ON pays.id=auteurs.nation 
  		WHERE pays.nom = nom_pays;
  	
 DECLARE CONTINUE HANDLER FOR NOT FOUND SET done=1;

 OPEN cur_auteur;

 WHILE done=0 DO 
  	FETCH cur_auteur INTO auth;
  	SET auteurs= concat_ws(',',auteurs,auth);
  END WHILE;
 
CLOSE cur_auteur;
END $
DELIMITER ;



