USE bibliotheque;

-- Création d'une vue
CREATE VIEW v_auteur_livre AS
SELECT livres.id,titre,concat_ws(' ',prenom,auteurs.nom) AS auteur,genres.nom
FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur 
INNER JOIN genres ON livres.genre=genres.id; -- genres.genre

-- Avec show table, une vue sera considérée comme une table
SHOW tables;

-- On peut exécuter des requêtes sur la vue
SELECT *FROM v_auteur_livre WHERE nom='Aventure';
SELECT *FROM v_auteur_livre WHERE auteur LIKE 'Step%';

-- Si on ajoute des données dans les tables
INSERT INTO livres (titre,annee,genre) VALUES
('Les misérables',1862,7);
INSERT INTO livre2auteur(id_auteur,id_livre) VALUES 
(33,143);

-- Elles sont mises à jour dynamiquement dans la vue
SELECT *FROM v_auteur_livre WHERE nom='Drame';

-- ALTER VIEW -> Modifier une vue (recréation)
ALTER VIEW v_auteur_livre AS 
SELECT livres.id,titre,concat_ws(' ',prenom,auteurs.nom) AS auteur
,genres.nom AS genre
,pays.nom AS nation 
FROM livres
INNER JOIN livre2auteur ON livres.id=livre2auteur.id_livre
INNER JOIN auteurs ON auteurs.id=livre2auteur.id_auteur 
INNER JOIN genres ON livres.genre=genres.id
INNER JOIN pays ON auteurs.nation=pays.id;

SELECT * FROM v_auteur_livre ORDER BY nation;

-- Supression de la vue
DROP VIEW v_auteur_livre ;
