USE exemple;

-- CHECK permet de limiter la plage de valeurs
CREATE TABLE personnes(
	id INT PRIMARY KEY AUTO_INCREMENT,
	prenom VARCHAR(50),
	nom VARCHAR(50) NOT NULL CHECK(CHAR_LENGTH(nom)>2 AND CHAR_LENGTH(nom)<=50),
	age INT CHECK(age>=0)
);

INSERT INTO personnes(prenom,nom,age) VALUES ('john','doe',34);

-- Erreur -> nom < à 3 caractères
INSERT INTO personnes(prenom,nom,age) VALUES 
('jane','doe',23),
('john','d',34);

-- Erreur -> age négatif
INSERT INTO personnes(prenom,nom,age) VALUES ('jane','doe',-23);

-- Gestion des clés étrangères avec ON DELETE, ON UPDATE
CREATE TABLE marques_cascade(
	id INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(60) NOT NULL,
	date_creation DATE
);

CREATE TABLE articles_cascade(
	reference INT PRIMARY KEY AUTO_INCREMENT,
	description VARCHAR(255),
	prix DECIMAL(6,2) NOT NULL DEFAULT 10.0, 				
	marque INT,
	-- On peut ajouter sur la contrainte de clé étrangère :
	-- ON DELETE CASCADE, ON UPDATE CASCADE, ON DELETE NULL ou ON UPDATE NULL 
	CONSTRAINT  FK_articles_marques_cascade
	FOREIGN KEY (marque)
	REFERENCES marques_cascade(id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO marques_cascade(nom) VALUES 
('Marque A'),
('Marque B'),
('Marque C');


INSERT INTO articles_cascade(description,prix,marque) VALUES 
('Souris',30.0,1),
('Clavier AZERTY',20.0,1),
('TV 4K',600.0,2),
('Carte mère ',130.0,3),
('Carte graphique',500.0,3);

SELECT * FROM marques_cascade;
SELECT * FROM articles_cascade;

-- ON DELETE CASCADE
-- Sans ON DELETE CASCADE, on ne peut pas supprimmer la MarqueA (id=1) tant que l'on n'a pas supprimé tous les articles qui ont pour clé étrangère 
-- la valeur marque=1
-- Avec ON DELETE CASCADE, La suppression de la marque id=1 est propagée au article id=1b et id=2
DELETE FROM marques_cascade WHERE id=1;
SELECT * FROM marques_cascade;
SELECT * FROM articles_cascade;

-- ON UPDATE CASCADE
-- avec ON UPDATE CASCADE la mise à jour de la clé primaire id 2 à 10 de la table marque est propagée
-- à la clé étrangère marque (2 à 10) de l'article id=3
UPDATE marque_cascades SET id=10 WHERE id=2;

SELECT * FROM marque_cascades;
SELECT * FROM articles_cascade;

ALTER TABLE articles_cascade DROP CONSTRAINT FK_articles_marques_cascade;

ALTER TABLE articles_cascade ADD CONSTRAINT FK_articles_marques_cascade
FOREIGN KEY(marque)
REFERENCES marque_cascades (id) ON DELETE SET NULL ON UPDATE SET NULL;

-- ON DELETE SET NULL
DELETE FROM marque_cascades WHERE id=3;
-- La suppression de la marque id=3 entraine la modification  à NULL des clés étrangères 
-- marque des articles id=4 et id=5

SELECT * FROM marque_cascades;
SELECT * FROM articles_cascade;

-- ON UPDATE SET NULL
UPDATE marque_cascades SET id=20 WHERE id=10;
-- la mise à jour de la clé primaire id 10 à 20
-- entraine la modification à NULL de la clé étrangère de l'article id=3

SELECT * FROM marque_cascades;
SELECT * FROM articles_cascade;


